function command_exists () {
    command -v "$1" &> /dev/null
}

function check_gpg() {
    command_exists gpg || echo "GPG is not installed. Run install_gpg";
}

function install_gpg() {
    if [[ "$OSTYPE" == "darwin"* ]]; then
        brew doctor
        brew install gnupg
        echo "GPG installed successfully. Need to create private / public gpg keys"
    fi
}

function check_git() {
    command_exists git || echo -e '\e]8;;https://git-scm.com/\aGit\e]8;;\a is not installed'
}

function install_git() {
    if [[ "$OSTYPE" == "darwin"* ]]; then
        brew doctor
        brew install git
        echo "Git installed successfully"
    fi
}

function check_jenv() {
    command_exists jenv || echo -e '\e]8;;https://www.jenv.be/\ajEnv\e]8;;\a is not installed'
}

function install_jenv() {
    if command_exists git; then
        export JENV_HOME="$HOME/.jenv"
        git clone https://github.com/jenv/jenv.git $JENV_HOME
        export PATH="$JENV_HOME/bin:$PATH"
        eval "$(jenv init -)"
        echo "jEnv installed successfully"
    fi
}

function check_dev_workspace() {
    [[ ! -d $HOME/Development ]] \
    && echo "Development workspace not created. Install the nbugash-zsh-plugin." \
    || export DEV_WORKSPACE="$HOME/Development"
}

function setup_dev_workspace() {
    [[ ! -d $HOME/Development ]] \
    && echo "Creating Development workspace" \
    && mkdir -p "$HOME/Development" \
    && export DEV_WORKSPACE="$HOME/Development" \
    && alias dev="cd $DEV_WORKSPACE"
}

function load_env() {
    current_dir=$(dirname "$0")
    if [[ -f "$current_dir/.env" ]]; then
        export $(cat "${current_dir}/.env" | xargs)
    fi
}

check_dev_workspace
check_gpg
check_git
check_jenv
