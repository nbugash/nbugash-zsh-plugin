# NBugash ZSH Plugin

## Installation
Clone repo to the zsh custom plugin directory

    git clone git@gitlab.com:nbugash/nbugash-zsh-plugin.git  ${ZSH_CUSTOM}/plugins/nbugash

Add `nbugash` to the zsh plugin in .zshrc file.